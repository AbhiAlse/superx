/*
 *Que 1: WAP to print the following pattern
Take input from the user
A B C D
1 3 5 7
A B C D
9 11 13 15
A B C D
 */

import java.util.*;
class Soln{
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the rows and cols");
		int row=sc.nextInt();
		int col=sc.nextInt();

		System.out.println("Enter the no");
		int no=sc.nextInt();

		System.out.println("Enter the Character");
		String str = sc.next();
		
		//char ch=str.charAt(0);

		for(int i=0;i<row;i++){
			char ch=str.charAt(0);

			for(int j=0;j<col;j++){

				if(i%2==0){
					System.out.print(" "+ch++);
				}
				else{
					System.out.print(" "+no);
					no=no+2;
				}
			}
			System.out.println();
		}

	}
}






