/*
 *Que 4: WAP to print the Perfect number in a given range.
Input: 1 to 50
Input: 40 to 100
 */
import java.util.*;


class Solution{

        static void checkNumber(int n){

                int sum = 0;

                for(int i=1;i<n;i++){

                        if(n%i==0)
                                sum=sum+i;
                }
                if(sum==n)
                        System.out.println(n);

        }

        public static void main(String [] args){
                Scanner sc=new Scanner(System.in);
                System.out.println("Enter the  Start number");
                int s=sc.nextInt();
                System.out.println("Enter the end number");
                int e=sc.nextInt();

                for(int i=s;i<=e;i++){
						
	      		checkNumber(i);
			
     		}

        }
}
