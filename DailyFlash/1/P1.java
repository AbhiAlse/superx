/*
 * Que 1 : WAP to print the following pattern
Take input from user
1 2 3 4
2 3 4 5
3 4 5 6
4 5 6 7

*/
import java.io.*;
class Demo{
	static void design(int rows,int col,int N){
		
		for(int i=0;i<rows;i++){	
			int No=N;	
			for(int j=0;j<col;j++){
				System.out.print(" "+No);
				No++;
			}
			System.out.println();
			N++;
		}
	

	}


	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the rows");
		int rows=Integer.parseInt(br.readLine());
		System.out.println("Enter the Columns");
		int col=Integer.parseInt(br.readLine());
		System.out.println("Enter the Number");
		int N=Integer.parseInt(br.readLine());

		design(rows,col,N);
	}
}
		

