/*
 * Check the alphabet of letters present in it
 */

import java.util.*;

class Demo {
    static int checkString(String str) {
        char[] arr = str.toCharArray();
        int flag = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]<65 ||(arr[i]>90&&arr[i]<97 ) || arr[i]>122 ) {
                flag = 1;
                break;
            }
        }
        return flag;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the string:");
        String str = sc.next();

        int flag = checkString(str);

        if (flag == 1) {
            System.out.println("String contains Non-alphabet letters.");
        } else {
            System.out.println("String contains Alphabet only.");
        }
    }
}

