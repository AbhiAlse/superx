/*
 *Que 5: WAP to replace vowels to # in given string
Input:Meta Data
output: M#t# D#t#
 */

class Soln{
	public static void main(String [] abhi){
		String str="Meta Data";
		char arr[]=str.toCharArray();

		for(int i=0;i<arr.length;i++){

			if(arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'||arr[i]=='A'||arr[i]=='E'||arr[i]=='I'||arr[i]=='O'||arr[i]=='U'){
				arr[i]=35;
			}
		}
		str=new String(arr);

		System.out.println(str);

	}
}
