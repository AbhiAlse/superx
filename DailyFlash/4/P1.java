/*
 *Que 1: WAP to print the following pattern
Take input from the user
A B C D
# # # #
A B C D
# # # #
A B C D
 */

import java.util.*;
class Demo{
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the rows and cols");

		int r=sc.nextInt();
		int c=sc.nextInt();


		for(int i=0;i<r;i++){
			char ch='A';

			for(int j=0;j<c;j++){
				if(i%2==0){
					System.out.print(""+ch);
					ch++;
				}
				else{
					System.out.print("#");
				}
			}
			System.out.println();
		}
	}
}



