/*
 *Que 1: WAP to print the following pattern
Take input from the user
1 2 3 4
a b c d
5 6 7 8
e f g h
9 10 11 12
 */

import java.util.*;
class Solution{
	public static void main(String [] args){

		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the rows ");
		int row=sc.nextInt();
		System.out.println("Enter the no");
		int no=sc.nextInt();

		char ch='a';

		for(int i=0;i<row;i++){

			for(int j=0;j<row;j++){
				if(i%2==0){
					System.out.print(" "+no++);
				}else{
					System.out.print(" "+((char)ch++));
				}
			}System.out.println();
		}
	}
}



