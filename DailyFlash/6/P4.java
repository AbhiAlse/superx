/*
 *Que 4: WAP to print strong numbers in a given range.
Input: 1 to 10
 */

import java.util.*;
class soln{

	static void checkNumber(int n){
		int sum=0;
		int temp=n;
		//int fact=1;
		while(n!=0){
			int fact=1;
			int rem=n%10;
			for(int i=1;i<=rem;i++){
				fact=fact*i;
			}sum=sum+fact;
			n=n/10;
		}
		if(sum==temp){
			System.out.println(temp);
		}		
	
	}

	public static void main(String [] args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the start no");
		int s=sc.nextInt();
		System.out.println("Enter the end no");
		int e=sc.nextInt();

		System.out.println("Strong numbers are :");
		for(int i=s;i<=e;i++){
			checkNumber(i);
		}

	}
}



