/*
 *Que 1 : WAP to print the following pattern
Take input from user
A B C D
B C D E
C D E F
D E F G
 */

import java.util.*;
class Demo{
	public static void main(String [] a){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter rows and cols");
		int rows=sc.nextInt();
		int col=sc.nextInt();

		char ch='A';

		for(int i=0;i<rows;i++){
			char ch2=ch;
			for(int j=0;j<col;j++){

				System.out.print(" "+ch2);
				ch2++;
			}
			ch++;
			System.out.println();
		}
	}
}



