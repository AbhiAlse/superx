/*
 *
 *Que 5 : WAP to count the size of given string
(without using inbuilt method)
 */

import java.util.*;
class Demo{
	static int stringSize(String str){
		char arr[]=str.toCharArray();
		int count=0;
		for(int i=0;i<arr.length;i++){
			count++;
	
		}
		return count;

	}

	public static void main(String []a ){
		Scanner sc=new Scanner(System.in);
		String str=sc.next();

		int size=stringSize(str);

		System.out.println("The size is "+size);

	}
}


