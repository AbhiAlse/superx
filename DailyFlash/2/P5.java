/*
 *
 *Que 5 : WAP to check whether the string contains vowels and return
the count of vowels.

 */
import java.util.*;
class Demo{
	static int checkVovel(String str){
		char arr[]=str.toCharArray();
		int count=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'||arr[i]=='A'||arr[i]=='E'||arr[i]=='I'||arr[i]=='O'||arr[i]=='U'){

				count++;
			}
		}
		return count;
	}
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		
		String str=sc.nextLine();
		int no = checkVovel(str);
		System.out.println(" Number of count is: "+no);
	}
}
		
				
		


