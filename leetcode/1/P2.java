/*
 *Search Insert Position (LeetCode 35)
Given a sorted array of distinct integers and a target value, return the index if the target
is found. If not, return the index where it would be if it were inserted in order.
You must write an algorithm with O(log n) runtime complexity.
Example 1:
Input: nums = [1,3,5,6], target = 5
Output: 2
Example 2:
Input: nums = [1,3,5,6], target = 2
Output: 1
Example 3:
Input: nums = [1,3,5,6], target = 7
Output: 4
Constraints:
1 <= nums.length <= 104
-104 <= nums[i] <= 104
nums contains distinct values sorted in ascending order.
-104 <= target <= 104
 */

import java.util.*;
class Solution{
	static int searchInsertPos(int arr[],int target){
		int index=-1;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==target||target<=arr[i]){	
				index=i;
				break;
			}
			if (target>arr[arr.length-1]){
				index=arr.length;
				break;
			}
		}
		return index;
	}

	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the array Size");
		int size=sc.nextInt();
		System.out.println("Enter the array Elements");
		int arr[]=new int[size];
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter the target Elements");
		int target=sc.nextInt();
		
		int index=searchInsertPos(arr,target);
		System.out.println("Target index= "+index);
		

	}
}
