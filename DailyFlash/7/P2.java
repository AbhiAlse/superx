/*
 *Que 2: WAP to print the following pattern
Take row input from the user
10
9 8
7 6 5
4 3 2 1
 */

class Solution{
	public static void main(String [] args){

		int n=10;
		int row=4;

		for(int i=0;i<row;i++){
			for(int j=0;j<=i;j++){

				System.out.print(" "+n--);
			}
		
			System.out.println();
		}
	}
}
