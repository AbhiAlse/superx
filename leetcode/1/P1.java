/*
 *Two Sum (LeetCode 1)
Given an array of integers nums and an integer target, return indices of the two
numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use
the same element twice.
You can return the answer in any order.
Example 1:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:
Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:
Input: nums = [3,3], target = 6
Output: [0,1]
Constraints:
2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
Only one valid answer exists.
 */

import java.util.*;

class Solution{
	static int[] twoSum(int arr[],int target){
		int id1=-1;
		int id2=-1;

		for(int i=0;i<arr.length;i++){

			for(int j=1+i ;j<arr.length;j++){
				
				if(arr[i]+arr[j]==target ){  //&& (j+1<arr.length) ){
					id1=i;
					id2=j;
					break;
				}
			}

		}
		
		return new int[]{id1,id2};
		

	}
	public static void main(String [] arghs){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of an array");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter the Elements of an array");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter the target value");
		int target=sc.nextInt();
		int index [] =twoSum(arr,target);
		System.out.print("[ ");
		for(int i=0;i<index.length;i++){
			System.out.print(index[i]+" ");
		}		
		System.out.println("]");

	}
}
