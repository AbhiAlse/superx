/*
 * Que 2 : WAP to print the following pattern
Take row input from user
1
1 2
2 3 4
4 5 6 7
*/
import java.io.*;
class Demo{
	static void design(int rows,int N){
		
		for(int i=0;i<rows;i++){		
			for(int j=0;j<=i;j++){
				System.out.print(" "+N);
				N++;
			}
			N--;
			System.out.println();
		}
	

	}


	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the rows");
		int rows=Integer.parseInt(br.readLine());
		System.out.println("Enter the Number");
		int N=Integer.parseInt(br.readLine());

		design(rows,N);
	}
}
		

