/*
   Que 3: WAP to check whether the given number is a strong number or not.
 */
import java.util.*;
class Demo{
	static void strong(int N){
		int temp=N;
		int Tsum=0;
		while(N!=0){
			int rem=N%10;
			int fact=1;
			for(int i=1;i<=rem;i++){
				fact=fact*i;
				
			}
			N=N/10;
			Tsum=Tsum+fact;
		}

		if(Tsum==temp){
			System.out.println("Strong number :"+temp);
		}else{
			System.out.println("Not Strong number :"+temp);
		}
	}
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		int N=sc.nextInt();	
		strong(N);
	}
}



