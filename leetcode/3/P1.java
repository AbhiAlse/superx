/*
1. To Lower Case (Leetcode-709)
Given a string s, return the string after replacing every uppercase
letter with the same lowercase letter.
Example 1:
Input: s = "Hello"
Output: "hello"
Example 2:
Input: s = "here"
Output: "here"
Example 3:
Input: s = "LOVELY"
Output: "lovely"

Constraints:
1 <= s.length <= 100
s consists of printable ASCII characters
*/
import java.util.*;

class Demo{
	static String toggle(String str){
		char arr[]=str.toCharArray();
		for(int i=0;i<arr.length;i++){
			if(arr[i]<=90&&arr[i]>=65){
				arr[i]+=32;
			}
			}
		str=new String (arr);

		
		return str;
	}
		

	public static void main(String [] args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String ");

		String str=sc.next();

		String str1=toggle(str);

		System.out.println(str1);

	}
}
