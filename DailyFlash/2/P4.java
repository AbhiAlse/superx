/*
 *Que 4 : WAP to print the composite numbers in the given range
Input: start:1
end:100
 */

import java.util.*;

class Demo{

	static void checkcomp(int S,int E){
	

		for(int i=S;i<=E;i++){
			int count=0;
			
			for(int j=1;j<=i;j++){

				if(i%j==0){
					count++;
				}

			}
			if(count>2){
				System.out.println("Composite No: "+i);
			}
		}
	}
	
	public static void main(String [] args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter start and end no");
		int S=sc.nextInt();
		int E=sc.nextInt();
		checkcomp(S,E);


		}
}
		
