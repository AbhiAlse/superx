/*
 *Que 5: WAP to print the occurrence of a letter in given String.
Input String: “Know the code till the core”
Alphabet : o
Output: 3
 */


import java.util.*;
class Demo{
	static int occurance(String str,char ch){
		char arr[]=str.toCharArray();
		int count=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==ch)
				count++;
		}
		return count;
	}
	

	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);

	
		String str="Know the code till the core";
		
		System.out.println("Enter the element");
		String str1=sc.next();

		char ch=str1.charAt(0);
		
		int count=occurance(str,ch);
		System.out.println(count);

	}
}



