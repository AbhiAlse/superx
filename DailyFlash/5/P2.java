/*
 *Que 2: WAP to print the following pattern
Take row input from the user
a
A B
a b c
A B C D
 */

import java.util.*;

class Demo{
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the rows");

	
		int r =sc.nextInt();
		
		for(int i=0;i<r;i++){
			char ch='a';
		
			for(int j=0;j<=i;j++){
				if(i%2==0){

					System.out.print(" "+ch++);
				}else{
				
					System.out.print(" "+ ((char)(ch-32)));
					ch++;
				}
			}
			System.out.println();
		}
	}
}




