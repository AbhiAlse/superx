/*
 *Que 3: WAP to check whether the given number is perfect or not.
 */
import java.util.*;


class soln{

	static void checkNumber(int n){

		int sum=0;
		for(int i=1;i<n;i++){
			if(n%i==0){
				sum=sum+i;
			}
		}
		if(sum==n){
			System.out.println("It is a perfect number");
		}else{
			System.out.println("It is not a perfect number");
		}
	}
	public static void main(String [] abhi){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter integer value");
		int n=sc.nextInt();
		
		checkNumber(n);
	}
}
