/*
 *Que 1: WAP to print the factorial of digits in a given range.
Input: 1-10
 */
import java.util.*;
class Demo{

	static void factorial(int S){
		int fact =1;
		
		for(int i=1;i<=S;i++){
			fact=fact*i;
		}

		System.out.println(fact);
	}

	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the start and end");

		int s=sc.nextInt();
		int E=sc.nextInt();

		for(int i=s;i<=E;i++){
			factorial(i);
		}
	}
}
