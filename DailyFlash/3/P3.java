/*
 *Que 3 : WAP to check whether the given no is a palindrome number or not.
 */
import java.util.*;
class Demo{

	static void checkPalindrome(int n){
		int temp=n;
		int res=0;
		
		while(n!=0){
			int rem=n%10;
			res=res*10+rem;
			n=n/10;
		}

		if(temp==res){
			System.out.println("Palindrome");
		}else{
			System.out.println("Not Palindrome");
		}	

	}
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		int n=sc.nextInt();
	
		checkPalindrome(n);

	}
}

		
