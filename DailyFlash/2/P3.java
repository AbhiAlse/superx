/*
 *Que 3 : WAP to check whether the given no is prime or composite
 */

import java.util.*;

class Demo{

	static void checkNumber(int N){
		int count=0;

		for(int i=1;i<=N;i++){
			if(N%i==0){
				count++;
			}
		}
		if(count==2){
			System.out.println("Prime No");
		}else{
			System.out.println("Composite number");
		}
	}
	public static void main(String [] args){

		Scanner sc=new Scanner(System.in);
		int N=sc.nextInt();
		checkNumber(N);


		}
}
		
