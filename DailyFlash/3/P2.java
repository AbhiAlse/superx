/*
 *Que 2 : WAP to print the following pattern
Take row input from user
1
2 1
3 2 1
4 3 2 1
 */
import java.util.*;

class Demo{
	public static void main(String [] args){
		
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter rows");
		int rows = sc.nextInt();
	
		System.out.println("Enter Number");
		int N = sc.nextInt();
		
			for(int i=0;i<rows;i++){
				int n=N;
				for(int j=0;j<=i;j++){
					System.out.print(" "+n);
					n--;
				}
				N=N+1;
				System.out.println();
			}
	}
}




