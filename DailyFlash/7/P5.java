/*
 *Que 5: WAP to find the last occurrence of a target in str.
Input → str : onomatopoeia

target : o
 */

class Solution{
	static void findlastOcc(String str,char target){
		int index=-1;
		char arr[]=str.toCharArray();
		for(int i=0;i<arr.length;i++){
			if(target==arr[i])
				index=i;
		}
		System.out.println(index);

	}
	public static void main(String [] args){
		String str="onomatopoeia";
		char target='o';
		findlastOcc(str,target);
	
	}
}
