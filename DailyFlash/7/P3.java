/*
 *Que 3: WAP to check whether the given number is Duck number or not.
 */
import java.util.*;

class Solution{
	static boolean checkNumber(int n){
		int flag=0;
		while(n!=0){
		
			if(n%10==0){
				System.out.println("Duck Number");
				flag=1;
				break;
				
			}
			n=n/10;
		}
		if(flag==1)
			return true;
		else
			return false;
			
		
	}

	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		int n=sc.nextInt();

		boolean ans = checkNumber(n);
		System.out.println(ans);

	}
}

	


