/*
 *Que 2 : WAP to print the following pattern
Take row input from user
1
2 4
3 6 9
4 8 12 16
 */

import java.util.*;
class Demo{
	public static void main(String [] a){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter rows");
		int rows=sc.nextInt();
		System.out.println("Enter No");
		int N=sc.nextInt();
				
		
		for(int i=0;i<rows;i++){
			int n=N;
			for(int j=0;j<=i;j++){

				System.out.print(" "+n);
				n=n+i+1;
			}
			N++;
			System.out.println();
		}
	}
}



