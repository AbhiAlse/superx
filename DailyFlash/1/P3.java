/* Que 3 : WAP to check whether the given no is odd or even
*/
import java.util.*;
class Demo{
	public static void main(String [] args){

		Scanner Sc=new Scanner (System.in);
		int x=Sc.nextInt();

		if(x%2==0)
			System.out.println(x+" number is even");
		else
			System.out.println(x+" number is odd");
	}
}
