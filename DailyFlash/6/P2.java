/*
 *Que 2: WAP to print the following pattern
Take row input from the user
1
7 26
63 124 215
342 511 728 999
 */

import java.util.*;

class Soln{
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the rows");
		int row=sc.nextInt();
		System.out.println("Enter the no");
		int no=sc.nextInt();

		for(int i=0;i<row;i++){

			for(int j=0;j<=i;j++){

				System.out.print(" "+ ((no*no*no)-1));
				no++;

			}
			System.out.println();
		}
	}
}


