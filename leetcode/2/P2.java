/*
 *Given two strings needle and haystack, return the index of the first occurrence of needle
in haystack, or -1 if needle is not part of haystack.
Example 1:
Input: haystack = "sadbutsad", needle = "sad"
Output: 0
Explanation: "sad" occurs at index 0 and 6.
The first occurrence is at index 0, so we return 0.
Example 2:
Input: haystack = "leetcode", needle = "leeto"
Output: -1
Explanation: "leeto" did not occur in "leetcode", so we return -1.

Constraints:
1 <= haystack.length, needle.length <= 104
haystack and needle consist of only lowercase English characters.
 */
import java.util.*;


class Solution{
	static int firstOcc(String haystack, String needle){
		
		char haystackarr[]=haystack.toCharArray();
		char needlearr[]=needle.toCharArray();
		

		int index=-1;
		
		
		for(int i=0;i<=haystackarr.length-needlearr.length;i++){
			int count=0;	
			for(int j=0;j<needlearr.length;j++){

				if(needlearr[j]==haystackarr[j+i]){
					
					count++;
				}else{
					break;
				}
			}				
			if(count==needlearr.length){
				index=i;
				break;
				
			}
			
		}												
		
		return index;
	}
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the haystack string");
		String haystack=sc.next();
		System.out.println("Enter the needle string");
		String needle =sc.next();
		
		int index=firstOcc(haystack,needle);
		System.out.println(index);
	}
}

