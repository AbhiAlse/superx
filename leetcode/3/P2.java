import java.util.*;
class Solution {
    static int[] plusOne(int[]arr){
		
	    for(int i=arr.length-1;i>=0;i--){
		    if(arr[i]<9){
			    arr[i]++;
			    return arr;
		    }
		    else{
			    arr[i]=0;
		    }
	    }
	
	    int arr2[]=new int[arr.length+1];

	    arr2[0]=1;

	    return arr2;


    } 
    public static void main(String [] args){
        
                Scanner sc=new Scanner(System.in);
                System.out.println("Enter the Size of an array");
                int size=sc.nextInt();
                System.out.println("Enter the Array elements");
                int arr[]=new int[size];
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

                int arr2[]=plusOne(arr);

                System.out.println("Converting it into the Array ");
                System.out.print("[");
                for(int x:arr2)
                        System.out.print(" "+x);

                System.out.println(" ]");




        }
}

