/*
 *1. Palindrome Number(LeetCode 9)
Given an integer x, return true if x is a palindrome, and false otherwise.
Example 1:
Input: x = 121
Output: true
Explanation: 121 reads as 121 from left to right and from right to left.
Example 2:
Input: x = -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-.
Therefore it is not a palindrome.
Example 3:
Input: x = 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.

Constraints:
-231 <= x <= 231 - 1
 */

import java.util.*;
class Solution{
	static boolean checkPalindrome(int n){
		int temp=n;
		int result=0;
		if (temp<0){
			return false;
		}

		while(n!=0){

			int rem=n%10;
			result=result*10+rem;
			n=n/10;
		}
		if(temp==result){
			return true;
		}else{
			return false;
		}
	}
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the integer");
		int n=sc.nextInt();
		boolean ans=checkPalindrome(n);

		System.out.println(ans);
		
	}
}
