/*
 *Que 4 : WAP to print each reverse numbers in the given range
Input: start:25435
end: 25449
 */
import java.util.*;
class Demo{

	static int printReverse(int n){
		int temp=n;
		int res=0;
		
		while(temp!=0){
			int rem=temp%10;
			res=res*10+rem;
			temp=temp/10;
		}
		return res;
	}
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Start number");
		int S=sc.nextInt();
		System.out.println("Enter the End number");
		int E=sc.nextInt();
		for(int i=S;i<=E;i++){
			int no = printReverse(i);

			System.out.println(no);
		}
	}
}

		
