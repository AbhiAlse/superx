/*
 *Que 4 : WAP to print the odd numbers in the given range
Input: start:1
end:10
 */

import java.util.*;
class Demo{
	public static void main(String [] abhi){
		Scanner sc=new Scanner(System.in);
		int s=sc.nextInt();
		int e=sc.nextInt();

		for(int i=s;i<=e;i++){
			if(i%2!=0)
				System.out.println(i);
		}	
	}
}
